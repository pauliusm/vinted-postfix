package "epel-release" do
  action :install
end

package "redis" do
  action :install
end

service "redis" do
  action [:enable, :start]
end

package "postfix" do
  action :install
end

bash "postconfi_redis" do
  code <<-END
	postconf -e 'mailbox_command=redis-cli -x lpush "$RECIPIENT"'
  END
end

