#!/usr/bin/env bats

@test "postfix is running" {
	run	postfix status
	[ "$status" -eq 0 ]
}
