#!/usr/bin/env bats

@test "redis is running" {
	run redis-cli ping
	[[ ${lines[0]} = "PONG" ]]
}

