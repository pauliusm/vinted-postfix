#!/usr/bin/env bats

@test "postfix is accepting email" {
        res="$(echo 'From: paulius@chroot.lt\nTo: test-redis-email' | sendmail redis@localhost.localdomain)"
        [[ "$res" -eq "" ]]
}

@test "email stored into redis" {
        run bash -c "redis-cli blpop redis@localhost.localdomain 1 | grep test-redis-email"
	[[ ${lines[0]} =~ "From: paulius@chroot.lt\nTo: test-redis-email" ]]
}

